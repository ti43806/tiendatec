import {Router} from 'express'
import { getElectronicos, saveElectronicos,updateElectronicos,deleteElectronicos } from '../Controllers/ElectronicosController.js'
import { subirImagen } from '../Middleware/Storage.js'
import { verificar } from '../Middleware/Auth.js'
const rutas = Router()

rutas.get('/api/electronicos',verificar, getElectronicos)
rutas.get('/api/electronicos/:id', getElectronicos)
rutas.post('/api/electronicos', subirImagen.single('imagen'), saveElectronicos)
rutas.put('/api/electronicos/:id', subirImagen.single('imagen'), updateElectronicos)
rutas.delete('/api/electronicos/:id', deleteElectronicos)
export default rutas