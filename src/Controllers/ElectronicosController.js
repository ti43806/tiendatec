import mongoose from "mongoose"
import * as fs from 'fs'
import { Deserializer } from "v8"

const esquema = new mongoose.Schema({
    nombre:String, imagen:String, categoria:String, marca:String, descripcion:String, precio:Number
},{versionKey:false})
const ElectronicosModel = new mongoose.model('electronicos',esquema)

export const getElectronicos = async(req,res) =>{
    try{
        const {id} = req.params
        const rows = 
        (id === undefined) ? await ElectronicosModel.find() : await ElectronicosModel.findById(id)
        return res.status(200).json({status:true, data:rows})
    }
    catch(error){
        return res.status(500).json({status:false, errors:[error]})
    }
}

export const saveElectronicos = async(req, res) =>{
    try{
        const {nombre,categoria,marca,descripcion,precio} = req.body
        const validacion = validar(nombre,categoria,marca,descripcion,precio,req.file,'Y')
        if(validacion == ''){
            const nuevoElectronicos = new ElectronicosModel({
                nombre:nombre, categoria:categoria,precio:precio,marca:marca,descripcion:descripcion,
                imagen:'/uploads/'+req.file.filename
            })
            return await nuevoElectronicos.save().then(
                () => {res.status(200).json({status:true,message:'Dispositivo guardado'})}
            )
        }
        else{
            return res.status(400).json({status:false,errors:validacion})
        }
    }
    catch(error){
        return res.status(500).json({status:false,errors:[error.message]})
    }
}
export const updateElectronicos = async(req, res) =>{
    try{
        const {id}=req.params
        const {nombre,categoria,marca,descripcion,precio} = req.body
        let imagen = ''
        let valores = {nombre:nombre, categoria:categoria,marca:marca, descripcion:descripcion, precio:precio}
        if(req.file != null){
            imagen = '/uploads/'+req.file.filename
            valores = {nombre:nombre, categoria:categoria, marca:marca, descripcion:descripcion, precio:precio,imagen:imagen}
            await eliminarImagen(id)
        }
        const validacion = validar(nombre,categoria,marca,descripcion,precio)
        if(validacion == ''){
            await ElectronicosModel.updateOne({_id:id},{$set: valores})
            return res.status(200).json({status:true,message:'Dispositivo actualizado'})
        }
        else{
            return res.status(400).json({status:false,errors:validacion})
        }
    }
    catch(error){
        return res.status(500).json({status:false,errors:[error.message]})
    }
}

export const deleteElectronicos = async(req,res) =>{
    try{
        const {id} = req.params
        await eliminarImagen(id)
        await ElectronicosModel.deleteOne({_id:id})
        return res.status(200).json({status:true,message:'Dispositivo eliminado'})
    }
    catch(error){
        returnres.status(500).json({status:false,errors:[error.message]})
    }
    }

const eliminarImagen = async(id) =>{
    const electronicos = await ElectronicosModel.findById(id)
    const img = electronicos.imagen
    fs.unlinkSync('./public/'+img)
}

const validar = (nombre,categoria,descripcion,marca,precio,img,sevalida)=>{
    var errors = []
    if(nombre === undefined || nombre.trim() === ''){
        errors.push('El nombre NO debe estar vacío.')
    }
    if(categoria === undefined || categoria.trim() === ''){
        errors.push('La categoría NO debe estar vacía.')
    }
    if(marca === undefined || marca.trim() === ''){
        errors.push('La marca NO debe estar vacía.')
    }
    if(descripcion === undefined || descripcion.trim() === ''){
        errors.push('La descripción NO debe estar vacía.')
    }
    if(precio === undefined || precio.trim() === ''|| isNaN(precio)){
        errors.push('El precio No debe estar vacío y debe ser númerico.')
    }
    if(sevalida === 'Y' && img === undefined){
        errors.push('Selecciona una imagen en formato png o jpg')
    }
    else{
        if(errors != ''){
            fs.unlinkSync('./public/uploads/'+img.filename)
        }
    }
    return errors

}